﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class parkingTechAnim : MonoBehaviour
{
    static Animator anim;
    static Animation turnAnimation;

    void Start()
    {
        anim = GetComponent<Animator>();
        anim.enabled = false;
    }

   
    public void clicktoPlay()
    {
        anim.enabled = true;
        anim.SetTrigger("isParking");
        StartCoroutine(LateCall());
    }

    public void clicktoStopParking()
    {
        anim.enabled = false;
    }

    IEnumerator LateCall()
    {
        yield return new WaitForSeconds(6);
        anim.enabled = false;
    }



}