﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class headlightSwitch : MonoBehaviour {
 
    
    public void changeColor()
    {
        Renderer rend = GetComponent<Renderer>();
        rend.material.shader = Shader.Find("Standard");
        rend.material.SetColor("_Color", Color.white);
    }

    public void changeBackColor()
    {
        Renderer rend = GetComponent<Renderer>();
        rend.material.shader = Shader.Find("Standard");
        rend.material.SetColor("_Color", Color.grey);
    }


}
