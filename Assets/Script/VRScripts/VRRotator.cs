﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class VRRotator : MonoBehaviour {

    public Transform myRig;
    public float speed = 0.3f;
    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update ()
    {
        if (OVRInput.GetDown(OVRInput.Button.Left, OVRInput.Controller.LTouch) 
            || OVRInput.GetDown(OVRInput.Button.Left, OVRInput.Controller.RTouch))
        {
            myRig.DORotate(Vector3.up * -45, speed, RotateMode.WorldAxisAdd);

        }

        if (OVRInput.GetDown(OVRInput.Button.Right, OVRInput.Controller.LTouch) 
            || OVRInput.GetDown(OVRInput.Button.Right, OVRInput.Controller.RTouch))
        {
            myRig.DORotate(Vector3.up * 45, speed, RotateMode.WorldAxisAdd);
           
        }
    }
}
