﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class RayScript : MonoBehaviour {

    public Transform thePlayer;
    public Transform[] targets = new Transform[3];

    [Range(0, 2)]
    public float tweenTime = 2;
    int index = 0;
    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    public void NextObject()
    {
        index++;
        if (index == targets.Length)
        {
            index = 0;
        }
        MovePlayer(targets[index]);
   

    }

    public void PreviousObject()
    {
        index--;
        if (index == -1)
        {
            index = targets.Length - 1;
        }
        MovePlayer(targets[index]);
    }



    
    void MovePlayer(Transform chg)
    {
        thePlayer.DOMove(chg.position, tweenTime);
        thePlayer.DORotateQuaternion(chg.rotation, tweenTime);
        thePlayer.DOLookAt(chg.forward, tweenTime);
    }
    public void ChangeIndex(int chg)
    {
        index = chg;
        MovePlayer(targets[index]);

    }
}