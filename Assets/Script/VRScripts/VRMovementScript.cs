﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VRMovementScript : MonoBehaviour {

    public CharacterController thePlayer;
    public Transform forwardDirection;
    public OVRInput.Controller myController;
    public OVRInput.Button myButton;
    public float speed;
    private Vector3 moveDirection;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {

        moveDirection = Vector3.zero;
        if (OVRInput.Get(myButton, myController))
        {
            moveDirection.z = 1;


        }



        //if (OVRInput.Get(OVRInput.Button.DpadDown, OVRInput.Controller.Remote))
        //{
        //    moveDirection.z = -1;

        //}

        //if (OVRInput.Get(OVRInput.Button.DpadLeft, OVRInput.Controller.Remote))
        //{
        //    moveDirection.x = -1;

        //}

        //if (OVRInput.Get(OVRInput.Button.DpadRight, OVRInput.Controller.Remote))
        //{
        //    moveDirection.x = 1;

        //}
        thePlayer.Move(forwardDirection.transform.TransformDirection(moveDirection) * Time.deltaTime * speed);
    }

    private void FixedUpdate()
    {
        
    }
}
