﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FaceMeMenuKeyDown : MonoBehaviour {

    public Transform theCamera;
    public float distance;
    public Transform VRCanvas;
    public UiManager UiManager;

    // Use this for initialization
	void Start () {
    }
	
	// Update is called once per frame
	void Update () {
        if (OVRInput.Get(OVRInput.Button.PrimaryThumbstick, OVRInput.Controller.LTouch))
        {
            MoveVRCanvas();

        }
        Ray ray = new Ray(theCamera.position, theCamera.forward); //debug, delete this
        Debug.DrawLine(ray.origin, ray.GetPoint(distance), Color.red);
	}

    public void MoveVRCanvas() {
        UiManager.categoryCanvas.enabled = true;
        Ray ray = new Ray(theCamera.position, theCamera.forward);
        Vector3 holder = ray.GetPoint(distance);
        holder.y = theCamera.position.y;
        VRCanvas.transform.position = holder;
        VRCanvas.LookAt(theCamera);

    } 
}
