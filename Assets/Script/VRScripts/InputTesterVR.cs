﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;


public class InputTesterVR : MonoBehaviour {
    public Vector2 PrimaryThumbstick;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        if (OVRInput.Get(OVRInput.Button.One, OVRInput.Controller.Remote))
        {
            
            Debug.Log("You pressed up");
        }
        PrimaryThumbstick = OVRInput.Get(OVRInput.Axis2D.PrimaryThumbstick, OVRInput.Controller.Gamepad);

    }
}
