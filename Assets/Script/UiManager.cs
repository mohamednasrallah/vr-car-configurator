﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UiManager : MonoBehaviour
{
    public wheelChanger wheelChanger;
    public CarObject CarObjectScript;
    public GameObject CarGameObject;
    public colorManager colorManager;

    [Header("Main Canvas")]
    public Canvas categoryCanvas;

    [Header("Right Pane Items")]
    public GameObject DiscoverOrb;
    public GameObject ConfiguratorOrb;
    public GameObject TechnologyOrb;

    [Header("Left Pane Items")]
    public GameObject InformationTab;

    [Header("Bottom Pane Items")]
    public GameObject AdaptiveOnSwitch;
    public GameObject AdaptiveOffSwitch;
    public GameObject ColorSelectionItems;
    public GameObject ChromeRimsButton;
    public GameObject PaintedRimsButton;
    public GameObject ToRegularPkgButton;
    public GameObject ToSportPackageButton;
    public GameObject SelectTireButtons;

    [Header("Center Pane Items")]   
    public GameObject NotAvailabeText;
    public GameObject ChangePromptText;
    public GameObject availabilityMessage;

    [Header("U.I Modifiable Objects")]
    public GameObject spoilerObject;
    public GameObject tireObject, chromeRimObject, paintedRimObject;
    public Material spoilerOriginalColor;
    public Material highlight;
    public Material ghosting;

    //bottom materials are for caching original colors
    Material tireColorOriginal;
    Material chromeRimColorOriginal;
    Material paintedRimColorOriginal;

    void Awake()
    {
        //disable parts of the u.i on wake. Will be re-enabled manually in according functions

        paintedRimObject.GetComponent<Renderer>().enabled = true;
        chromeRimObject.GetComponent<Renderer>().enabled = true;
        spoilerObject.GetComponent<Renderer>().enabled = true;
        tireObject.GetComponent<Renderer>().enabled = true;

        tireColorOriginal = tireObject.GetComponent<Renderer>().sharedMaterial;
        chromeRimColorOriginal = chromeRimObject.GetComponent<Renderer>().sharedMaterial;
        paintedRimColorOriginal = paintedRimObject.GetComponent<Renderer>().sharedMaterial;
        spoilerObject.GetComponent<Renderer>().enabled = true;
        tireObject.GetComponent<Renderer>().enabled = true;

        NotAvailabeText.SetActive(false);
        ChangePromptText.SetActive(false);

    categoryCanvas.enabled = true; //Makes entire U.I visible. This is already true by default but here for future code reuse/modify/debugging

        DiscoverOrb.SetActive(false);       //canvas visibility
        ConfiguratorOrb.SetActive(false);
        TechnologyOrb.SetActive(false);

        ColorSelectionItems.SetActive(false);

        InformationTab.SetActive(false); //gameobject visibility, specifically the information box

        ChromeRimsButton.SetActive(false);
        PaintedRimsButton.SetActive(false);
        ToRegularPkgButton.SetActive(false);
        ToSportPackageButton.SetActive(false);
        AdaptiveOnSwitch.SetActive(false);
        AdaptiveOffSwitch.SetActive(false);
        SelectTireButtons.SetActive(false);


    }

    public void discoverButton()
    {
        //sets all conditions for how the discover tab behaves

        ColorSelectionItems.SetActive(false);
        SelectTireButtons.SetActive(false);
        AdaptiveOnSwitch.SetActive(false);
        AdaptiveOffSwitch.SetActive(false);
        PaintedRimsButton.SetActive(false);
        ChromeRimsButton.SetActive(false);
        ToRegularPkgButton.SetActive(false);
        ToSportPackageButton.SetActive(false);

        spoilerObject.GetComponent<Renderer>().sharedMaterial = spoilerOriginalColor;


        if (DiscoverOrb.activeSelf == false)
        {

            DiscoverOrb.SetActive(true);
            ConfiguratorOrb.SetActive(false);
            TechnologyOrb.SetActive(false);
        }
        else
            DiscoverOrb.SetActive(false);
    }       //Category

    public void suspension()
    {
        SelectTireButtons.SetActive(false);
        spoilerObject.GetComponent<Renderer>().sharedMaterial = spoilerOriginalColor;

        tireObject.GetComponent<Renderer>().sharedMaterial = ghosting;
        chromeRimObject.GetComponent<Renderer>().sharedMaterial = ghosting;
        paintedRimObject.GetComponent<Renderer>().sharedMaterial = ghosting;


    }

    public void performanceTires()
    {

        spoilerObject.GetComponent<Renderer>().sharedMaterial = spoilerOriginalColor;

        tireObject.GetComponent<Renderer>().sharedMaterial = tireColorOriginal;
        chromeRimObject.GetComponent<Renderer>().sharedMaterial = chromeRimColorOriginal;
        paintedRimObject.GetComponent<Renderer>().sharedMaterial = paintedRimColorOriginal;

        SelectTireButtons.SetActive(true);

    }

    public void aerodynamics()
    {
        SelectTireButtons.SetActive(false);
        spoilerObject.GetComponent<Renderer>().sharedMaterial = highlight;
        tireObject.GetComponent<Renderer>().sharedMaterial = tireColorOriginal;
        chromeRimObject.GetComponent<Renderer>().sharedMaterial = chromeRimColorOriginal;
        paintedRimObject.GetComponent<Renderer>().sharedMaterial = paintedRimColorOriginal;

    }


    public void configuratorButton()
    {
        //sets all conditions for how the config tab behaves

        spoilerObject.GetComponent<Renderer>().sharedMaterial = spoilerOriginalColor;

        AdaptiveOnSwitch.SetActive(false);
        AdaptiveOffSwitch.SetActive(false);
        SelectTireButtons.SetActive(false);
        PaintedRimsButton.SetActive(false);
        ChromeRimsButton.SetActive(false);

        tireObject.GetComponent<Renderer>().sharedMaterial = tireColorOriginal;
        chromeRimObject.GetComponent<Renderer>().sharedMaterial = chromeRimColorOriginal;
        paintedRimObject.GetComponent<Renderer>().sharedMaterial = paintedRimColorOriginal;

        ToRegularPkgButton.SetActive(false);
        ToSportPackageButton.SetActive(false);



        if (ConfiguratorOrb.activeSelf == false)
        {

            DiscoverOrb.SetActive(false);
            ConfiguratorOrb.SetActive(true);
            TechnologyOrb.SetActive(false);


        }
        else
            ConfiguratorOrb.SetActive(false);

    }      //Category

    public void paintChanger()
    {
                PaintedRimsButton.SetActive(false);
                ChromeRimsButton.SetActive(false);
                ToRegularPkgButton.SetActive(false);
                ToSportPackageButton.SetActive(false);

                ColorSelectionItems.SetActive(true);
    }

    public void changeRims()
    {
                ColorSelectionItems.SetActive(false);
                ToRegularPkgButton.SetActive(false);
                ToSportPackageButton.SetActive(false);

                PaintedRimsButton.SetActive(true);
                ChromeRimsButton.SetActive(true);
    }


    public void changePackage()
            {
                ColorSelectionItems.SetActive(false);

                PaintedRimsButton.SetActive(false);
                ChromeRimsButton.SetActive(false);

                ToRegularPkgButton.SetActive(true);
                ToSportPackageButton.SetActive(true);
            }


    public void technologyButton()
    {
        //sets all conditions for how the tech tab behaves
        AdaptiveOnSwitch.SetActive(false);
        AdaptiveOffSwitch.SetActive(false);
        ColorSelectionItems.SetActive(false);
        SelectTireButtons.SetActive(false);
        PaintedRimsButton.SetActive(false);
        ChromeRimsButton.SetActive(false);
        ToRegularPkgButton.SetActive(false);
        ToSportPackageButton.SetActive(false);

        spoilerObject.GetComponent<Renderer>().sharedMaterial = spoilerOriginalColor;

        tireObject.GetComponent<Renderer>().sharedMaterial = tireColorOriginal;
        chromeRimObject.GetComponent<Renderer>().sharedMaterial = chromeRimColorOriginal;
        paintedRimObject.GetComponent<Renderer>().sharedMaterial = paintedRimColorOriginal;


        if (TechnologyOrb.activeSelf == false)
        { //turn on if off

            DiscoverOrb.SetActive(false);
            ConfiguratorOrb.SetActive(false);
            TechnologyOrb.SetActive(true);

            AdaptiveOnSwitch.SetActive(false);
            AdaptiveOffSwitch.SetActive(false);
        }
        else
            TechnologyOrb.SetActive(false);
    }       //Category

    public void driveTech()
{
    //reserved space for driveTech section to keep script format
}

    public void parkingTech()
    {
        AdaptiveOnSwitch.SetActive(false);
        AdaptiveOffSwitch.SetActive(false);

    }

    public void AdaptiveHeadlights()
    { 
        AdaptiveOnSwitch.SetActive(true);
        AdaptiveOffSwitch.SetActive(true);
    }

    public void NotAvailable()
    {
        availabilityMessage.SetActive(true);
        StartCoroutine(AvailablilityLateCall());
    }

    public void RemoveDialogBox()
    {
        ChangePromptText.SetActive(false);

    }

    public void ClickedYes()
    {
        if (colorManager.redClick == true)
        {
            CarGameObject.GetComponent<Renderer>().sharedMaterial = CarObjectScript.RedColor;
            wheelChanger.toPaintedRims();

            RemoveDialogBox();
        

    }
        if (colorManager.greenClick == true)
        {
            CarGameObject.GetComponent<Renderer>().sharedMaterial = CarObjectScript.GreenColor;
            wheelChanger.toChromeRims();

            RemoveDialogBox();                
        }

        if (colorManager.greyClick == true)
            {
            CarGameObject.GetComponent<Renderer>().sharedMaterial = CarObjectScript.GreyColor;
            wheelChanger.toPaintedRims();

            RemoveDialogBox();                      
        }

    }

    public void CloseMenuButton() {
        categoryCanvas.enabled = false;
    }

    IEnumerator AvailablilityLateCall() //times out the "Not Available" Message
    {
        yield return new WaitForSeconds(2);
        availabilityMessage.SetActive(false);
    }
}