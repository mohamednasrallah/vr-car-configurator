﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraMover : MonoBehaviour
{

    public CameraCubeVanilla camCube;

    // Use this for initialization
    public void SuspensionCamera()
    {

        camCube.zoomStart = 2;
        camCube.zoomMin = 1;
        camCube.zoomMax = 3;

        camCube.yMinLimit = 5;
        camCube.yMaxLimit = 75;
        camCube.xMinLimit = -361;
        camCube.xMaxLimit = 360;

        camCube.MoveCamera(new Vector3(-1.5f, 0f, 1.2f)); //new Vector3(f, f, f);

    }

    public void SpoilerCamera()
    {
        camCube.zoomStart = 3;
        camCube.zoomMin = 2;
        camCube.zoomMax = 5;

        camCube.MoveCamera (new Vector3(0f, 1.0f, -3f)); //new Vector3(f, f, f);

    }
    
    public void resetCameraMain()
    {
        camCube.zoomStart = 7;
        camCube.zoomMin = 6;
        camCube.zoomMax = 15;

        camCube.yMinLimit = 5;
        camCube.yMaxLimit = 75;
        camCube.xMinLimit = -361;
        camCube.xMaxLimit = 360;


        camCube.MoveCamera(new Vector3(0f, 0.12f, -0.5f)); //new Vector3(f, f, f);
    }
    
    public void tireDFCamera()
    {
      
        camCube.yMinLimit = 0;
        camCube.yMaxLimit = 15;
        camCube.xMinLimit = 50;
        camCube.xMaxLimit = 70;
        camCube.zoomMin = 3;
        camCube.zoomMax = 5;
        
    }
    public void tireDRCamera()
    {
        camCube.yMinLimit = 15;
        camCube.yMaxLimit = 15;
        camCube.xMinLimit = 30;
        camCube.xMaxLimit = 90;
        camCube.zoomMin = 2;
        camCube.zoomMax = 2;
    }
    public void tirePFCamera()
    {
        camCube.yMinLimit = 5;
        camCube.yMaxLimit = 75;
        camCube.xMinLimit = -96;
        camCube.xMaxLimit = 0;
        camCube.zoomMin = 7;
        camCube.zoomMax = 10;

    }
    public void tirePRCamera()
    {
        camCube.yMinLimit = 0;
        camCube.yMaxLimit = 80;
        camCube.xMinLimit = -361;
        camCube.xMaxLimit = 360;
        camCube.zoomMin = 3;
        camCube.zoomMax = 15;
    }

    /*•	Driver Front
o	Y Min = 0 / Y Max = 15
o	X Min = 50 / X Max = 70
o	Zoom Min = 3 / Zoom Max = 5
•	Driver Rear
o	Y Min = 15 / Y Max = 15
o	X Min = 30 / X Max = 90
o	Zoom Min = 2 / Zoom Max = 2
•	Passenger Front
o	Y Min = 5 / Y Max = 75
o	X Min = -96 / X Max = 0
o	Zoom Min = 7 / Zoom Max = 10
•	Passenger Rear
o	Y Min = 0 / Y Max = 80
o	Full 360
o	Zoom Min = 3 / Zoom Max = 15*/
}
