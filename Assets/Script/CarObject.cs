﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CarObject : MonoBehaviour {

    public Material RedColor, BlueColor, YellowColor, GreenColor, GreyColor;
    [HideInInspector]
    public Material[] ColorSelection = new Material[5];
    [Header("Select Chrome Object First, Then Painted")]
    public GameObject[] RimSelection = new GameObject[2];   //take in two game objects (2 different rims) to choose one of them

    public GameObject PackageSelectionAntenna; //to turn antenna on or off for sports package


    void Awake()
    {
        ColorSelection[0] = RedColor;
        ColorSelection[1] = BlueColor;
        ColorSelection[2] = YellowColor;
        ColorSelection[3] = GreenColor;
        ColorSelection[4] = GreyColor;
    }

}