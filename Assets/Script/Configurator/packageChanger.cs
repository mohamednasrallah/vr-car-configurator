﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class packageChanger : MonoBehaviour
{
    public UiManager UiManager;
    public colorManager colorManager;
    public wheelChanger wheelChanger;
    public CarObject carObjectScript;
    public GameObject car;


    [HideInInspector]
    public bool RegularPackageOn = true, SportPackageOn;


    public void toSportPackage()
    {
        if ((car.GetComponent<Renderer>().sharedMaterial == carObjectScript.BlueColor ||
            car.GetComponent<Renderer>().sharedMaterial == carObjectScript.YellowColor ||
            car.GetComponent<Renderer>().sharedMaterial == carObjectScript.GreenColor)
            && (wheelChanger.chromeWheels == true))
        {
            carObjectScript.PackageSelectionAntenna.SetActive(true);
            SportPackageOn = true;
            RegularPackageOn = false;
            Debug.Log("sportspackage is on");
        }
        else
        {
            UiManager.NotAvailable();
        }
    }

    public void toRegularPackage()
    {

        carObjectScript.PackageSelectionAntenna.SetActive(false);
        SportPackageOn = false;
        RegularPackageOn = true;
    }
}