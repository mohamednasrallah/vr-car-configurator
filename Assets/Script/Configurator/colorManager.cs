﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class colorManager : MonoBehaviour {

    public wheelChanger wheelChanger;
    public UiManager UiManager;
    public CarObject CarObjectScript;
    public GameObject car;

    [HideInInspector]
    public bool redClick, blueClick, yellowClick, greenClick, greyClick; //flags for "Yes" button in U.I prompting color change options' change

    void Start() {

        blueClick = true;
    }


    public void ColorSelectRed()
    {
        redClick = true;
        blueClick = false;
        yellowClick = false;
        greenClick = false;
        greyClick = false;
        Debug.Log("redClick saved");

       if (wheelChanger.paintedWheels == true)
        {
            car.GetComponent<Renderer>().sharedMaterial = CarObjectScript.RedColor;
            wheelChanger.toPaintedRims();

        }

        if (wheelChanger.chromeWheels == true)
        {
            UiManager.ChangePromptText.SetActive(enabled); 
        }
    }

    public void ColorSelectBlue()
    {
        redClick = false;
        blueClick = true;
        yellowClick = false;
        greenClick = false;
        greyClick = false;
        Debug.Log("blueClick saved");
        car.GetComponent<Renderer>().sharedMaterial = CarObjectScript.BlueColor;

    }

    public void ColorSelectYellow()
    { 
        redClick = false;
        blueClick = false;
        yellowClick = true;
        greenClick = false;
        greyClick = false;
        Debug.Log("yellowClick saved");
        car.GetComponent<Renderer>().sharedMaterial = CarObjectScript.YellowColor;
    }

    public void ColorSelectGreen()
    {
        redClick = false;
        blueClick = false;
        yellowClick = false;
        greenClick = true;
        greyClick = false;

        if (wheelChanger.chromeWheels == true)
        {
            car.GetComponent<Renderer>().sharedMaterial = CarObjectScript.GreenColor;
            wheelChanger.toChromeRims();
            Debug.Log("greenClick saved");
        }

        if (wheelChanger.paintedWheels == true)
        {
            UiManager.ChangePromptText.SetActive(enabled);
        }
    }

    public void ColorSelectGrey()
    {
        redClick = false;
        blueClick = false;
        yellowClick = false;
        greenClick = false;
        greyClick = true;

        if (wheelChanger.paintedWheels == true)
        {
            car.GetComponent<Renderer>().sharedMaterial = CarObjectScript.GreyColor;
            wheelChanger.toPaintedRims();
            Debug.Log("greyClick saved");
        }
        if (wheelChanger.chromeWheels == true)
        {
            UiManager.ChangePromptText.SetActive(enabled);

        }
    }


}
