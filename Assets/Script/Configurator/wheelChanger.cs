﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class wheelChanger : MonoBehaviour {

    public colorManager colorManager;
    public UiManager UiManager;
    public packageChanger packageChanger;
    public CarObject carObjectScript;
    public GameObject car;

    [HideInInspector]
    public bool chromeWheels, paintedWheels;
    private void Awake()
    {
        chromeWheels = true;
        paintedWheels = false;
    }

    public void toChromeRims() {

        if (car.GetComponent<Renderer>().sharedMaterial == carObjectScript.BlueColor ||
            car.GetComponent<Renderer>().sharedMaterial == carObjectScript.YellowColor ||
            car.GetComponent<Renderer>().sharedMaterial == carObjectScript.GreenColor)
        {
            chromeWheels = true;
            paintedWheels = false;

            carObjectScript.RimSelection[0].SetActive(true);//chrome rims object reference
            carObjectScript.RimSelection[1].SetActive(false);//painted rims object reference
        }

        else
        {
            UiManager.NotAvailable();
        }
    }

    public void toPaintedRims() {
        if ((car.GetComponent<Renderer>().sharedMaterial != carObjectScript.GreenColor) && (packageChanger.SportPackageOn == false))
        {
            chromeWheels = false;
            paintedWheels = true;
            carObjectScript.RimSelection[1].SetActive(true); //painted rims object reference
            carObjectScript.RimSelection[0].SetActive(false); //chrome rims object reference
        }
        else
        {
            UiManager.NotAvailable();
        }

    }
}
