﻿using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;

public class OculusControllerInput : BaseInputModule
{
    public static OculusControllerInput Instance;

    [Header(" [Cursor setup]")]
    public Sprite CursorSprite; //Sprite to use for UI System
    public bool swapCursorOnButton = false;
    public Sprite CursorOnButton;
    public Material CursorMaterial; //Material is not Required;
    public float NormalCursorScale = 0.00025f;

    [Header(" [Input Settings]")]
    public float easeValue = .1f; 
    public bool AllowMouseEvents;

    [Header(" [Oculus Settings]")]
    public bool useRemote;      //Override UI To use Remote 
    public Transform theCamera; //Insert Oculus Camera Here for Remote Settings
    public OVRInput.Button ClickButton;

    //Drag Left and Right Controllers Here!
    public Transform LeftController;
    public Transform RightController;

    [Header(" [Runtime variables]")]
    [Tooltip("Indicates whether or not the gui was hit by any controller this frame")]
    public bool GuiHit;
    [Tooltip("Indicates whether or not a button was used this frame")]
    public bool ButtonUsed;


    [Tooltip("Generated Cursors")]
    public RectTransform[] Cursors;
    private GameObject[] CurrentPoint;
    private GameObject[] CurrentPressed;
    private GameObject[] CurrentDragging;
    private PointerEventData[] PointEvents;
    private bool Initialized = false;
    [Tooltip("Generated non rendering camera (used for raycasting ui)")]
    private Camera ControllerCamera;
    private Transform[] Controllers;

    protected override void Start()
    {
        base.Start();
        if(AllowMouseEvents)
        {
            if(!GetComponent<DualInputModule>())
            {
                AllowMouseEvents = false;
                Debug.LogError("DualInputNotFound! Disabling Allowing Mouse Events");
            }
        }
        if (Initialized == false)
        {
            Instance = this;
            //Set controllers for Raycasting
            Controllers = new Transform[2];
            Controllers[0] = LeftController;
            Controllers[1] = RightController;

            //OverRide for Remote set both controllers to Camera Easier then rewritting 
            //This does create two cursors on top of each other but we can fix it later.
            Cursors = new RectTransform[2];
            if (useRemote)
            {
                Cursors = new RectTransform[1];
                Controllers = new Transform[1];
                Controllers[0] = theCamera;
                //Controllers[1] = theCamera;
            }

            //Generate Controller Camera this Camera does not Render it just creates a plane
            ControllerCamera = new GameObject("Controller UI Camera").AddComponent<Camera>();
            ControllerCamera.clearFlags = CameraClearFlags.Nothing; //CameraClearFlags.Depth;
            ControllerCamera.cullingMask = 0; // 1 << LayerMask.NameToLayer("UI"); 
            ControllerCamera.nearClipPlane = .001f;
            //Important KEY! You must turn off StereoTarget in order for it to render correctly;
            ControllerCamera.stereoTargetEye = StereoTargetEyeMask.None;

            //Create Cursors


            for (int index = 0; index < Cursors.Length; index++)
            {
                GameObject cursor = new GameObject("Cursor " + index);
                Canvas canvas = cursor.AddComponent<Canvas>();
                cursor.AddComponent<CanvasRenderer>();
                cursor.AddComponent<CanvasScaler>();
                cursor.AddComponent<UIIgnoreRaycast>();
                cursor.AddComponent<GraphicRaycaster>();
                //ADDED CODE
                cursor.layer = LayerMask.NameToLayer("UI");
                canvas.renderMode = RenderMode.WorldSpace;
                canvas.sortingOrder = 1000; //set to be on top of everything

                Image image = cursor.AddComponent<Image>();
                image.sprite = CursorSprite;
                image.material = CursorMaterial;


                if (CursorSprite == null)
                    Debug.LogError("Set CursorSprite on " + this.gameObject.name + " to the sprite you want to use as your cursor.", this.gameObject);

                Cursors[index] = cursor.GetComponent<RectTransform>();
            }

            CurrentPoint = new GameObject[Cursors.Length];
            CurrentPressed = new GameObject[Cursors.Length];
            CurrentDragging = new GameObject[Cursors.Length];
            PointEvents = new PointerEventData[Cursors.Length];

            Canvas[] canvases = GameObject.FindObjectsOfType<Canvas>();
            foreach (Canvas canvas in canvases)
            {
                canvas.worldCamera = ControllerCamera;
            }
            //Is setup!
            Initialized = true;
        }
    }

    // Use screen midpoint as locked pointer location, enabling look location to be the "mouse"
    private bool GetLookPointerEventData(int index)
    {
        if (PointEvents[index] == null)
            PointEvents[index] = new PointerEventData(base.eventSystem);
        else
            PointEvents[index].Reset();

        PointEvents[index].delta = Vector2.zero;
        PointEvents[index].position = new Vector2(Screen.width / 2, Screen.height / 2);
        PointEvents[index].scrollDelta = Vector2.zero;

        base.eventSystem.RaycastAll(PointEvents[index], m_RaycastResultCache);
        PointEvents[index].pointerCurrentRaycast = FindFirstRaycast(m_RaycastResultCache);
        if (PointEvents[index].pointerCurrentRaycast.gameObject != null)
        {
            if (swapCursorOnButton)
            {
                if (PointEvents[index].pointerCurrentRaycast.gameObject.GetComponent<Button>())
                {
                    Cursors[index].GetComponent<Image>().sprite = CursorOnButton;
                }
                else
                {
                    Cursors[index].GetComponent<Image>().sprite = CursorSprite;
                }
            }
            GuiHit = true; //gets set to false at the beginning of the process event
        }
        else
        {
            if (swapCursorOnButton)
            {
                Cursors[index].GetComponent<Image>().sprite = CursorSprite;
            }
        }
        
        m_RaycastResultCache.Clear();

        return true;
    }

    // Update the cursor location and whether it is enabled
    // This code is based on Unity's DragMe.cs code provided in the UI drag and drop example
    private void UpdateCursor(int index, PointerEventData pointData)
    {
        if (PointEvents[index].pointerCurrentRaycast.gameObject != null)
        {
            Cursors[index].gameObject.SetActive(true);

            if (pointData.pointerEnter != null)
            {
                RectTransform draggingPlane = pointData.pointerEnter.GetComponent<RectTransform>();
                Vector3 globalLookPos;
                if (RectTransformUtility.ScreenPointToWorldPointInRectangle(draggingPlane, pointData.position, pointData.enterEventCamera, out globalLookPos))
                {
                    //nonEased Cursors;
                    //Cursors[index].position = globalLookPos;
                    //Cursors[index].rotation = draggingPlane.rotation;
                    Cursors[index].DOMove(globalLookPos,easeValue);
                    Cursors[index].DORotateQuaternion(draggingPlane.rotation, easeValue);
                    // scale cursor based on distance to camera
                    float lookPointDistance = (Cursors[index].position - Camera.main.transform.position).magnitude;
                    float cursorScale = lookPointDistance * NormalCursorScale;
                    if (cursorScale < NormalCursorScale)
                    {
                        cursorScale = NormalCursorScale;
                    }

                    Cursors[index].localScale = Vector3.one * cursorScale;
                }
            }
        }
        else
        {
            Cursors[index].gameObject.SetActive(false);
        }
    }

    // Clear the current selection
    public void ClearSelection()
    {
        if (base.eventSystem.currentSelectedGameObject)
        {
            base.eventSystem.SetSelectedGameObject(null);
        }
    }

    // Select a game object in the event system
    private void Select(GameObject go)
    {
        ClearSelection();

        if (ExecuteEvents.GetEventHandler<ISelectHandler>(go))
        {
            base.eventSystem.SetSelectedGameObject(go);
        }
    }

    // Send update event to selected object
    // Needed for InputField to receive keyboard input / as well as DropDown Menus
    private bool SendUpdateEventToSelectedObject()
    {
        if (base.eventSystem.currentSelectedGameObject == null)
            return false;

        BaseEventData data = GetBaseEventData();

        ExecuteEvents.Execute(base.eventSystem.currentSelectedGameObject, data, ExecuteEvents.updateSelectedHandler);

        return data.used;
    }

    //Convert the CameraPosition to Controller Setup
    private void UpdateCameraPosition(int index)
    {
        ControllerCamera.transform.position = Controllers[index].transform.position;
        ControllerCamera.transform.forward = Controllers[index].transform.forward;
    }

    //Only needed for SteamVR
    private void InitializeControllers()
    {
        //for (int index = 0; index < Controllers.Length; index++)
        //{
        //    if (Controllers[index] != null && Controllers[index].index != SteamVR_TrackedObject.EIndex.None)
        //    {
        //        ControllerDevices[index] = SteamVR_Controller.Input((int)Controllers[index].index);
        //    }
        //    else
        //    {
        //        ControllerDevices[index] = null;
        //    }
        //}
    }

    // Process is called by UI system to process events
    public override void Process()
    {
        InitializeControllers();
        GuiHit = false;
        ButtonUsed = false;
        // send update events if there is a selected object - this is important for InputField to receive keyboard events
        SendUpdateEventToSelectedObject();

        // see if there is a UI element that is currently being looked at
        for (int index = 0; index < Cursors.Length; index++)
        {
            if (Controllers[index].gameObject.activeInHierarchy == false)
            {
                if (Cursors[index].gameObject.activeInHierarchy == true)
                {
                    Cursors[index].gameObject.SetActive(false);
                }
                continue;
            }

            UpdateCameraPosition(index);

            bool hit = GetLookPointerEventData(index);
            if (hit == false)
                continue;

            CurrentPoint[index] = PointEvents[index].pointerCurrentRaycast.gameObject;

            // handle enter and exit events (highlight)
            base.HandlePointerExitAndEnter(PointEvents[index], CurrentPoint[index]);

            // update cursor
            UpdateCursor(index, PointEvents[index]);

            if (Controllers[index] != null)
            {
                if (ButtonDown(index))
                {
                    ClearSelection();

                    PointEvents[index].pressPosition = PointEvents[index].position;
                    PointEvents[index].pointerPressRaycast = PointEvents[index].pointerCurrentRaycast;
                    PointEvents[index].pointerPress = null;

                    if (CurrentPoint[index] != null)
                    {
                        CurrentPressed[index] = CurrentPoint[index];

                        GameObject newPressed = ExecuteEvents.ExecuteHierarchy(CurrentPressed[index], PointEvents[index], ExecuteEvents.pointerDownHandler);

                        if (newPressed == null)
                        {
                            // some UI elements might only have click handler and not pointer down handler
                            newPressed = ExecuteEvents.ExecuteHierarchy(CurrentPressed[index], PointEvents[index], ExecuteEvents.pointerClickHandler);
                            if (newPressed != null)
                            {
                                CurrentPressed[index] = newPressed;
                            }
                        }
                        else
                        {
                            CurrentPressed[index] = newPressed;
                            // we want to do click on button down at same time, unlike regular mouse processing
                            // which does click when mouse goes up over same object it went down on
                            // reason to do this is head tracking might be jittery and this makes it easier to click buttons
                            ExecuteEvents.Execute(newPressed, PointEvents[index], ExecuteEvents.pointerClickHandler);
                        }

                        if (newPressed != null)
                        {
                            PointEvents[index].pointerPress = newPressed;
                            CurrentPressed[index] = newPressed;
                            Select(CurrentPressed[index]);
                            ButtonUsed = true;
                        }

                        ExecuteEvents.Execute(CurrentPressed[index], PointEvents[index], ExecuteEvents.beginDragHandler);
                        PointEvents[index].pointerDrag = CurrentPressed[index];
                        CurrentDragging[index] = CurrentPressed[index];
                    }
                }
                
                if (ButtonUp(index))
                {
                    if (CurrentDragging[index])
                    {
                        ExecuteEvents.Execute(CurrentDragging[index], PointEvents[index], ExecuteEvents.endDragHandler);
                        if (CurrentPoint[index] != null)
                        {
                            ExecuteEvents.ExecuteHierarchy(CurrentPoint[index], PointEvents[index], ExecuteEvents.dropHandler);
                        }
                        PointEvents[index].pointerDrag = null;
                        CurrentDragging[index] = null;
                    }
                    if (CurrentPressed[index])
                    {
                        ExecuteEvents.Execute(CurrentPressed[index], PointEvents[index], ExecuteEvents.pointerUpHandler);
                        PointEvents[index].rawPointerPress = null;
                        PointEvents[index].pointerPress = null;
                        CurrentPressed[index] = null;
                    }
                }

                // drag handling
                if (CurrentDragging[index] != null)
                {
                    ExecuteEvents.Execute(CurrentDragging[index], PointEvents[index], ExecuteEvents.dragHandler);
                }
            }
        }
        if (AllowMouseEvents)
        {
            GetComponent<DualInputModule>().Process();
        }
    }

    //###################################
    //Input Section for ButtonDown and Button you can see the hidden Steam Code Below
    //###################################
    private bool ButtonDown(int index)
    {
        //Remote Override can also be rewritten for controllers
        if(useRemote)
        {
            return (OVRInput.GetDown(OVRInput.Button.One, OVRInput.Controller.Remote));
        }
        if (index == 0)
        {
            return (OVRInput.GetDown(ClickButton, OVRInput.Controller.LTouch));
        }
        else
        {
            return (OVRInput.GetDown(ClickButton, OVRInput.Controller.RTouch));
        }

        //return (ControllerDevices[index] != null && ControllerDevices[index].GetPressDown(Valve.VR.EVRButtonId.k_EButton_SteamVR_Trigger) == true);
    }

    private bool ButtonUp(int index)
    {
        //Remote Override can also be rewritten for controllers
        if (useRemote)
        {
            return (OVRInput.GetUp(OVRInput.Button.One, OVRInput.Controller.Remote));
        }
        if (index == 0)
        {
            return (OVRInput.GetUp(ClickButton, OVRInput.Controller.LTouch));
        }
        else
        {
            return (OVRInput.GetUp(ClickButton, OVRInput.Controller.RTouch));
        }
        //return (ControllerDevices[index] != null && ControllerDevices[index].GetPressUp(Valve.VR.EVRButtonId.k_EButton_SteamVR_Trigger) == true);
    }
}
